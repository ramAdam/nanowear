import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';



const list  =[
              {
                title: 'React',
                url: 'https://reactjs.org/',
                author: 'Jordan Walke',
                num_comments: 3,
                points: 4,
                objectID: 0,


              },
              {
                title: 'Redux',
                url: 'https://redux.js.org/',
                author: 'Dan Abramov, Andrew Clark',
                num_comments: 2,
                points: 5,
                objectID: 1,
              },
];


function display(item){
  return <div key={item.objectID}>
              
              <span>
                <a href={item.url}>{item.title}</a>
              </span>
              <span>{item.author}</span>
              <span>{item.num_comments}</span>
              <span>{item.points}</span>
              
          
        </div>
}

const element = <div className='App'>{list.map(display)}</div>

class App extends Component {

  state = {
    counter: 0,
  };

  onIncrement = () => {
    this.setState((state) => ({ counter: state.counter + 1}));
  }

  onDecrement = () => {
    this.setState((state) => ({ counter: state.counter -1 }));
  }

  render() {

  
    return (
              
            <div className='App'>
              <p>{this.state.counter}</p>
              <button onClick={this.onIncrement} type="button">increase</button>
              <button onClick={this.onDecrement} type="button">decrease</button>
            </div>
      
    
        
      
    );
  }
}

export default App;


  